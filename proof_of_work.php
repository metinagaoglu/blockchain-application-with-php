<?php

	class ProofOfWork
	{
		const TARGET_ZEROS = 5;
		const MAX_NONCE = PHP_INT_MAX;

		public $block;

		public function __construct(Block $block) {
			$this->block = $block;
		}

		public function prepare(int $nonce) {
			return $this->block->prevBlockHash.
				$this->block->data.
				dechex($this->block->timestamp).
				dechex($this->block->targetZeros).
				dechex($nonce);
		}

		public function run()
		{
			$nonce = 0;

			echo "Mining the block containing {$this->block->data}".PHP_EOL;

			while ($nonce < self::MAX_NONCE ) {
				$data = $this->prepare($nonce);

				$hash = hash('sha256',$data);

				if ($this->satisfiesTarget($hash)) {
					$this->block->nonce = $nonce;
					$this->block->hash = $hash;

					return $this->block;
				}

				$nonce++;
			}

			throw new MiningException("Could not mine the block");
		}

		public function satisfiesTarget($hash)
		{
			return substr($hash, 0, self::TARGET_ZEROS) === str_repeat('0', self::TARGET_ZEROS);
		}
	}
