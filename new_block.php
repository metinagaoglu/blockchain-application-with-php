<?php

	function newBlock(string $data,$prevBlockHash) {
		$block = (new Block(time(),$data,$prevBlockHash));

		return (new ProofOfWork($block))->run();
	}
