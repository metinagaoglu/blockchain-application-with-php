<?php
	include 'block.php';
	include 'new_block.php';
	include 'blockchain.php';
    include 'proof_of_work.php';
    include 'CLI.php';

    $blockchain = new Blockchain();
    $cli = new CLI($blockchain);
    $cli->run($argv);